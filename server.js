const express = require("express");
const dotEnv = require("dotenv");
const path = require('path');
const twofish = require("./twofish");
const app = express();

dotEnv.config({path: "./config.env"});

// app.set('views',path.join(__dirname, 'index.html'));
// app.set('view engine', 'ejs');

app.use("/twofish", twofish);

app.listen(process.env.PORT || 3000, () => {
    console.clear();
    console.log(`Server running on port ${process.env.PORT}`);
})