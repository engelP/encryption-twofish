const chilkat = require('@chilkat/ck-node12-win64');

const chilkatExample = () => {

    const crypt = new chilkat.Crypt2();

    // Set el encryption algorithm = "twofish"	
    crypt.CryptAlgorithm = "twofish";

    // CipherMode puede ser "ecb" or "cbc"
    crypt.CipherMode = "cbc";

    // KeyLength puede ser 128, 192, 256
    crypt.KeyLength = 256;

    // El PaddingScheme determina el contenido de los bytes
    // que se agregan para rellenar el resultado a un múltiplo del
    // tamaño del bloque del algoritmo de cifrado.  Twofish tiene un bloque
    // tamaño de 16 bytes, por lo que la salida cifrada siempre es
    // un múltiplo de 16.
    crypt.PaddingScheme = 0;

    // EncodingMode especifica la codificación de la salida para
    // cifrado y la entrada para el descifrado.
    // Puede ser "hexadecial", "url", "base64" o "citado-imprimible".
    crypt.EncodingMode = "hex";

    // Se requiere un vector de inicialización si se utiliza el modo CBC.
    // El modo BCE no utiliza un IV.
    // La longitud del IV es igual al tamaño del bloque del algoritmo.
    const ivHex = "000102030405060708090A0B0C0D0E0F";
    crypt.SetEncodedIV(ivHex,"hex");

    // La clave secreta debe ser igual al tamaño de la clave.  Para
    // Cifrado de 256 bits, la clave secreta binaria es de 32 bytes.
    // Para el cifrado de 128 bits, la clave secreta binaria es de 16 bytes.
    const keyHex = "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F";
    crypt.SetEncodedKey(keyHex,"hex");

    const encStr = crypt.EncryptStringENC("probando");
    console.log(encStr);

    const decStr = crypt.DecryptStringENC(encStr);
    console.log(decStr);

}

module.exports = chilkatExample;